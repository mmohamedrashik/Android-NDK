# ANDROID NDK 
#### 1.To Create H FILE 

```cmd
javah -d jni -classpath E:\android-sdk\platforms\android-25\android.jar;E:\android-sdk\extras\android\support\v7\appcompat\libs\android-support-v7-appcompat.jar;E:\android-sdk\extras\android\support\v7\appcompat\libs\android-support-v4.jar;../../build\intermediates\classes\debug guy.droid.im.ctest.MainActivity
```
#### 2 CREATE C FILE

#### 3 IN -> local.properties 
```gradle
	ndk.dir=E\:\\android-ndk
	sdk.dir=E\:\\android-sdk
```
#### 4.IN -> Build.gradle inside default config
```gradle
   		ndk
                {
                    moduleName "HelloJNI" libname
                 }
```		 
#### 5. IN -> Build.gradle outside default config
```gradle
            sourceSets.main
            {
                jni.srcDirs = [] /** should call ndk.build cmd**/
                jniLibs.srcDir 'src/main/libs'
            }
```	    
#### 6. IN CPP -FOLDER

*	create Android.mk
*	create Application.mk

#### 7. In android commond line execute ndk-build
```cmd
C:\Project\ndk-build.cmd
```

#### 8. Continue... SEP APK DIFF Processors

https://code.tutsplus.com/tutorials/advanced-android-getting-started-with-the-ndk--mobile-2152

https://www.youtube.com/watch?v=kFtxo7rr2HQ
