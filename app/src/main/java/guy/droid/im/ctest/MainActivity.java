package guy.droid.im.ctest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.textv);
        textView.setText(HelloJNI());
    }

    public native String HelloJNI();

    static {
        System.loadLibrary("HelloJNI");
    }
}
